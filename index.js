// 1. Create variables to store to the following user details:

const firstName = "James Anthony";
const lastName = "Suico";
let myAge = 27;
const myHobbies = ["Watching Anime", "Watching Youtube", "Watching Netflix"];
const myAddress = {
	houseNumber: "T-029",
			street: "North Road Jagobiao",
			city: "Mandaue",
			state: "Cebu"
		};

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + myAge);
console.log("Hobbies: ");
console.log(myHobbies);
console.log("Work Address: ");
console.log(myAddress);



/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullNamebucky = "Bucky Barnes";
	console.log("My bestfriend is: " + fullNamebucky);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);